# Ansible Role: Kubecolor

[![pipeline status](https://gitlab.com/enmanuelmoreira/ansible-role-kubecolor/badges/main/pipeline.svg)](https://gitlab.com/enmanuelmoreira/ansible-role-kubecolor/-/commits/main)

This role installs [Kubecolor](https://github.com/hidetatz/kubecolor) binary on any supported host.

## Requirements

github3.py >= 1.0.0a3

It can be installed from pip:

```bash
pip install github3.py
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    kubecolor_version: latest # v0.0.19 if you want a specific version
    kubecolor_arch: x86_64 # x86_64, arm64 or ppc64le
    setup_dir: /tmp
    kubecolor_bin_path: /usr/local/bin/kubecolor
    kubecolor_repo_path: https://github.com/hidetatz/kubecolor/releases/download

This role can install the latest or a specific version. See [available kubecolor releases](https://github.com/instrumenta/kubecolor/releases/) and change this variable accordingly.

    kubecolor_version: latest # v0.0.19 if you want a specific version

The path which kubecolor will be downloaded.

    kubecolor_repo_path: https://github.com/instrumenta/kubecolor/download

The location where the kubecolor binary will be installed.

    kubecolor_bin_path: /usr/local/bin/kubecolor

kubecolor supports x86_64, arm64 and ppc64le CPU architectures, just change for the main architecture of your CPU

    kubecolor_arch: x86_64 # x86_64, arm64 or ppc64le

Kubecolor needs a GitHub token so it can be downloaded. You must use the **github_token** variable in `vars/main.yml` in order to install Kubecolor. However, this value can be empty until GitHub reaches the maximum number of anonymous connections.

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: kubecolor

## License

MIT / BSD
